#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <linux/delay.h>

#include "xr20m1170_lib.h"

// Hardware clock frequency, not a tuning parameter!
#define CLOCK_FREQ 64000000
// Clock prescalar can be set to 1 or 4, but is left at 1. Not sent over SPI yet!
#define CLK_PRESCALAR 1
// Valid sampling modes are 4, 8, or 16. Sent over SPI.
#define SAMPLING_MODE 16

// Fixed point math, don't change without checking the Excel sheet
// Centered around 32 bit ints, and not needing to hold a larger number than
// CLOCK_FREQ defined above
#define FIXED_POINT_LEVEL 6
#define FRACTION_MASK 63
#define ROUNDUP_THRES 32

int init_serial(struct spi_device *spi_device, unsigned int XR20M1170_reset_gpio, int baudrate)
{
    ssize_t spiReadResult;
    int returnCode;
    uint8_t dataToSend[2] = {0, 0};

    uint32_t requiredDivisor;
    uint8_t dlm_reg;
    uint8_t dll_reg;
    uint8_t divisorFractionFixedPoint;
    uint8_t dld_reg;

    if (baudrate <= 0)
    {
        pr_alert("RS485: XR20M1170 init with invalid baudrate %d, defaulting to 115200\n",
                baudrate);
        baudrate = 115200;
    }

    // Calculate the baud rate registers
    // Kernel has no support for floating point calculations! Using fixed point
    // calculations. Don't try to merge calculations, have had issues with this
    // not calculating correctly.
    requiredDivisor = ((CLOCK_FREQ / CLK_PRESCALAR) << FIXED_POINT_LEVEL);
    requiredDivisor /= (baudrate * SAMPLING_MODE);

    dlm_reg = requiredDivisor >> (8 + FIXED_POINT_LEVEL);
    dll_reg = (requiredDivisor >> FIXED_POINT_LEVEL) & 0xFF;

    divisorFractionFixedPoint = requiredDivisor & FRACTION_MASK;
    dld_reg = divisorFractionFixedPoint >> (FIXED_POINT_LEVEL - 4);
    if (divisorFractionFixedPoint > ROUNDUP_THRES)
    {
        // TODO: This isn't correct in all cases!
        dld_reg++;
    }
    dld_reg &= 0x0F;

    // Append flags for bit sampling modes
    if (SAMPLING_MODE == 4)
    {
        dld_reg |= FOURX_MODE;
    }
    else if (SAMPLING_MODE == 8)
    {
        dld_reg |= EIGHTX_MODE;
    }
    // else 16x Mode, DLD[5:4] = 00

    printk("RS485: XR20M1170 init with baudrate %d, giving DLM %d, DLL %d, DLD %d\n",
            baudrate, dlm_reg, dll_reg, dld_reg);

    // Run a reset sequence
    gpio_set_value(XR20M1170_reset_gpio, 0);
    udelay(10);
    gpio_set_value(XR20M1170_reset_gpio, 1);
    udelay(10);

    // Setup of chip
    // NOTE:    Ordering is extermely important! Register addresses are shared
    //          and multiplexed based on the status of LCR and EFR[4], and to
    //          some extent MCR[2]. Read the datasheet before changing!
    if (dld_reg != 0)
    {
        // LCR = 0xBF Access special registers
        // Ignore all others, will overwrite later
        dataToSend[0] = WRITE_ADR | LCR_ADR;
        dataToSend[1] = 0xBF;
        spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }

        // Enable enhanced registers, only possible while LCR = 0xBF
        // Set EFR[4] = 1
        dataToSend[0] = WRITE_ADR | EFR_ADR;
        dataToSend[1] = EFR_EN;
        returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }

        // LCR[7] = 1 (Baud Rate Divisors Enable, and other setup registers)
        // Ignore all others, will overwrite later
        dataToSend[0] = WRITE_ADR | LCR_ADR;
        dataToSend[1] = 0b10000000;
        returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }

        // DLD can only be accessed when EFR[4] = 1
        // Set DLD, values determined above
        dataToSend[0] = WRITE_ADR | DLD_ADR;
        dataToSend[1] = dld_reg;
        returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }

        // LCR = 0xBF Access special registers
        // Ignore all others, will overwrite later
        dataToSend[0] = WRITE_ADR | LCR_ADR;
        dataToSend[1] = 0xBF;
        spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }

        // Disable enhanced registers, only possible while LCR = 0xBF
        // Set EFR[4] = 0
        dataToSend[0] = WRITE_ADR | EFR_ADR;
        dataToSend[1] = 0;
        returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }
    }

    // LCR[7] = 1 (Baud Rate Divisors Enable, and other setup registers)
    // Ignore all others, will overwrite later
    dataToSend[0] = WRITE_ADR | LCR_ADR;
    dataToSend[1] = 0b10000000;
    returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
    if (returnCode < 0) {
        pr_alert("RS485: FAILED to write to SPI device!\n");
        return -ENODEV;
    }

    // Baud rate registers can only be accessed when LCR[7] = 1, and LCR != 0xBF
    if (dlm_reg != 0)
    {
        dataToSend[0] = WRITE_ADR | DLM_ADR;
        dataToSend[1] = dlm_reg;
        returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
        if (returnCode < 0) {
            pr_alert("RS485: FAILED to write to SPI device!\n");
            return -ENODEV;
        }
    }

    dataToSend[0] = WRITE_ADR | DLL_ADR;
    dataToSend[1] = dll_reg;
    returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
    if (returnCode < 0) {
        pr_alert("RS485: FAILED to write to SPI device!\n");
        return -ENODEV;
    }

    // Set word size, parity, and stop bits, quit setup of registers
    // LCR[7] = 0 (Data registers enable)
    // LCR[6] = 0 (transmit break)
    // LCR[5] = 0 (no forced parity)
    // LCR[4] = 0 (odd parity)
    // LCR[3] = 0 (no parity generation/check)
    // LCR[2] = 1 (2 stop bits)
    // LCR[1:0] = 11 (8-bit word)
    dataToSend[0] = WRITE_ADR | LCR_ADR;
    dataToSend[1] = 0b00000111;
    returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
    if (returnCode < 0) {
        pr_alert("RS485: FAILED to write to SPI device!\n");
        return -ENODEV;
    }

    // EFCR can only be accessed when LCR[7] = 0
    // Set automatic RS-485 half-duplex direction output enable/disable EFCR[4] = 1
    // Invert polarity of the RTS# output pin by setting EFCR[5] = 1
    dataToSend[0] = WRITE_ADR | EFCR_ADR;
    dataToSend[1] = 0b00110000;
    returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
    if (returnCode < 0) {
        pr_alert("RS485: FAILED to write to SPI device!\n");
        return -ENODEV;
    }

    // Read back the data just written, to verify that the device is actually connected.
    // EFCR_ADR can only be accessed when LCR[7] = 0
    dataToSend[0] = READ_ADR | EFCR_ADR;
    spiReadResult = spi_w8r8(spi_device, dataToSend[0]);
    if (spiReadResult < 0) {
        pr_alert("RS485: FAILED to read back data written to SPI device.\n");
        return -ENODEV;
    }
    else if ((uint8_t)spiReadResult != dataToSend[1]) {
        pr_alert("RS485: Incorrect data read back from the device, %d\n", (uint8_t)spiReadResult);
        return -ENODEV;
    }

    // FCR can only be accessed when LCR[7] = 0
    // Trigger RX interrupt when FIFO has 16 bytes
    // Trigger TX interrupt when FIFO has 56 empty bytes
    dataToSend[0] = WRITE_ADR | FCR_ADR;
    dataToSend[1] = (0b01 << FCR_RX_OFFSET) | (0b11 << FCR_TX_OFFSET) | FCR_FIFO_EN;
    returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
    if (returnCode < 0) {
        pr_alert("RS485: FAILED to write to SPI device!\n");
        return -ENODEV;
    }

    return enable_tx_interrupt(spi_device, 0);
}

int serial_transmit_bytes(struct spi_device *spi_device, uint8_t* buf, uint8_t length)
{
    ssize_t spiReadResult;
    uint8_t dataToSend[65] = {0};

    dataToSend[0] = (WRITE_ADR | THR_ADR);
    memcpy(&dataToSend[1], buf, length);

    spiReadResult = spi_write(spi_device, dataToSend, length + 1);
    if (spiReadResult < 0) {
        pr_alert("RS485: FAILED to write to SPI device!\n");
        return -EFAULT;
    }

    return 0;
}

int enable_tx_interrupt(struct spi_device *spi_device, uint8_t enable)
{
    int returnCode;
    uint8_t dataToSend[2] = {0, 0};

    // IER can only be accessed when LCR[7] = 0
    // IER[0] = 1 (RX data int enable)
    // IER[1] = 1 (TX data empty enable)
    dataToSend[0] = WRITE_ADR | IER_ADR;

    if (enable) {
        dataToSend[1] = IER_RHR | IER_THR;
    }
    else {
        dataToSend[1] = IER_RHR;
    }

    returnCode = spi_write(spi_device, &dataToSend, sizeof(dataToSend));
    if (returnCode < 0) {
        pr_alert("RS485: FAILED to write to SPI device from enable_tx_interrupt!\n");
        return -EFAULT;
    }

    return 0;
}

ssize_t get_rx_fifo_level(struct spi_device *spi_device)
{
    // RXLVL can only be accessed when LCR[7] = 0
    uint8_t readAdr = READ_ADR | RXLVL_ADR;
    return spi_w8r8(spi_device, readAdr);
}

int read_rx_bytes(struct spi_device *spi_device, uint8_t* bufToWrite, uint8_t readLength)
{
    struct spi_message message;
    struct spi_transfer x;
    uint8_t *sendBuffer;
    uint8_t *recvBuffer;
    int ret;

    // For portability, a 32-byte limit might be required. Investigate.
    // Subtract one byte for the dummy byte read when transferring data.
    if (readLength > 31) {
        readLength = 31;
    }

    // Allocate DMA-safe space for tx and rx buffers, both 3 bytes long
    sendBuffer = kmalloc(readLength + 1, GFP_KERNEL);
    recvBuffer = kmalloc(readLength + 1, GFP_KERNEL);
    if (!sendBuffer || !recvBuffer) {
        pr_alert("RS485: Memory allocation failure attempting to read RX buffer!\n");
        return -ENOMEM;
    }

    memset(sendBuffer, 0, readLength + 1);
    sendBuffer[0] = (uint8_t)(READ_ADR | RHR_ADR);

    // Set up SPI message with a single bi-directional transfer
    spi_message_init(&message);
    memset(&x, 0, sizeof(x));
    x.len = readLength + 1;
    x.tx_buf = sendBuffer;
    x.rx_buf = recvBuffer;
    spi_message_add_tail(&x, &message);

    // Do the I/O
    ret = spi_sync(spi_device, &message);
    if (ret < 0) {
        pr_alert("RS485: Read error while reading RX buffer!\n");
        kfree(sendBuffer);
        kfree(recvBuffer);
        return ret;
    }

    // Read buffer starts at recvBuffer, but first byte is the dummy byte read
    // while outputting the register number.
    memcpy(bufToWrite, &recvBuffer[1], readLength);
    kfree(sendBuffer);
    kfree(recvBuffer);
    return readLength;
}
