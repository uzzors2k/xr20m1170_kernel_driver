#ifndef _GPIOHELPER_H_
#define _GPIOHELPER_H_

#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

MODULE_LICENSE("GPL");

int init_gpioControl(struct gpio gpioControl[], size_t num,
    irq_handler_t interrupt_handler, int irq_index);

void cleanup_Gpio(struct gpio gpioControl[], size_t num);

#endif
