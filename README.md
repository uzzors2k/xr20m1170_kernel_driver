# README #

This is a kernel module character driver for an [XR20M1170 UART chip](https://www.maxlinear.com/product/interface/uarts/i2c-spi-uarts/xr20m1170), using SPI communication with a Raspberry Pi. Currently used in an RS-485 transceiver, intended for an underwater ROV. Should be easy to use in any RS-485 application once complete, and eventually any application requiring a dedicated UART, provided different hardware is built around the XRM20M1170 chip.

KiCad schematic and PCB design files are included, along with Gerber files for PCB production. A bill of materials, and assembly drawings are also provided. The PCB fits onto a Raspberry Pi Zero format, but will also fit on the regular Raspberry Pi units as well.

### Usage ###

Compile the SPIdev disabling device tree file, found under the deviceTree directory:

    dtc -@ -I dts -O dtb -o spi_disabler.dtbo spi_disabler.dts

Load the SPIdev disabling device tree module:

    sudo dtoverlay -d . spi_disabler

Compile the source, and pick out the rs485_ch.ko file. Load it using

    sudo insmod rs485_ch.ko

and unload using

    sudo rmmod rs485_ch.ko

Check that the driver was loaded correctly using

    sudo dmesg


Read e.g. 29 bytes from device. Will block until enough bytes are present

    head -c29 /dev/rs485_ch

Write a string to the device

    echo "test String writing" > /dev/rs485_ch

Write a 128 character long string to the device, demonstrating splitting of messages

    echo "abcdefghijklmnopqrstuvwxyzzyxwvutsrqponmlkjihgfedcba0123456789_?abcdefghijklmnopqrstuvwxyzzyxwvutsrqponmlkjihgfedcba0123456789_?" > /dev/rs485_ch

### Speed Test Scripts ###

I've written a set of Python scripts for automated baud rate testing. A host computer communicates to two identical client scripts running on the machines where the baudrate test takes place. The clients are connected to each-other with a RS-485 line, and on the same network as the host computer. The host will then instruct the clients to take a master role in turns, and have the master send a known data packet to the slave, which the slave decodes. After each test iteration, the baudrate is increased, and run again. The script terminates when incorrect data is received.

The script will hang if the incorrect number of bytes are received. This is caused by the baudrate being too high, and requires a manual termination. A timeout in the client scripts should be implemented to automatically detect incomplete data.

### Version History ###

**Remaining work and features**
- Streamline loading of the device tree overlay. Make a proper custom device tree, laying claim to all required IO pins.
- Blocking of multiple access to the driver, via the open and release calls? Is this really desired?
- Bit level error handling
- Standard TTY interface option?

**Version 0.7.1**
Fixed a bug in the RX interrupt handler when a buffer overflow occurred.

**Version 0.7.0**
Fixed an error when sending data spanning several hardware buffers. Improved data reception speed. Fixed a bug in the speed test scripts. Removed excessive debugging prints.

**Version 0.6.6**
Created Python utility for stress testing the baudrate in a system. Discovered that sending data exceeding a hardware buffer length seems to fault. Unsure if driver fault, or test utility fault.

**Version 0.6.5**
ioctl option for changing the SPI clock rate internally on the board.

**Version 0.6.4**
Added Makefile targets for most Raspberry Pi platforms. Updated documentation for how to enable driver at boot time.

**Version 0.6.3**
Proper support for blocking and non-blocking read/write operations. Previous bug where unread messages are overwritten has been corrected.

**Version 0.6.2**
Changing baudrate over the ioctl interface works!

**Version 0.6.1**
Added ioctl interface for sending parameters to the driver. Included a small command line tool demonstrating this usage.

**Initial commit, version 0.6.0**
Communication works correctly using internal HW buffers. Error/feature where new messages received will overwrite old ones, rather than append to the read buffer. Writing to the character driver is non-blocking, and will throw an error if the HW TX buffer is not empty.


### How do I get set up? ###

This module can be cross-compiled against the raspbian linux kernel, on a Ubuntu system. Place this project in a directory tree where the kernel source folder is located at ../../linux for platforms Raspberry Pi 1, Pi Zero, Pi Zero W, and Compute Module. For platforms Raspberry Pi 2, Pi 3, Pi 3+, and Compute Module 3 place the kernel source at ../../linux_v2 This is mostly so different versions of this kernel driver can be compiled at the same time.

Summary of steps provided here, alternatively see [Kernel building](https://www.raspberrypi.org/documentation/linux/kernel/building.md) from the Raspberry Pi Foundation.

1) Install various dependencies:

    sudo apt install git bc bison flex libssl-dev make libc6-dev libncurses5-dev

2) Install the 32-bit toolchain:

    sudo apt install crossbuild-essential-armhf

3) Locate the correct kernel version for your operating system. The available kernels
    can be browsed at: https://github.com/raspberrypi/linux/branches The active kernel
    version can be checked using the command:

    uname -r

    I must admit this hasn't always worked however, and I have had to resort to
    downloading the latest kernel version from git, followed by a rpi-update to
    get matching kernel versions...

4a) To cross-compile for the Raspberry Pi 1, Pi Zero, Pi Zero W, and Compute Module,
    enter the directory with the source code, and setup the configuration:

    KERNEL=kernel
    make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcmrpi_defconfig

4b) To cross-compile for the Raspberry Pi 2, Pi 3, Pi 3+, and Compute Module 3,
    enter the directory with the source code, and setup the configuration:

    KERNEL=kernel7
    make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcm2709_defconfig

5) Finally compile the kernel!
    make -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs


After this the kernel module should compile using these object files.
Keep in mind that local raspberry pi makefiles need to be lowercase, whereas Ubuntu Makefiles need uppercase!

6) Move back to the driver source directory, and build using one of the make targets, rp1, rp2, rpz, or rp3

### Integrating into a distro ###

The spidev disabler needs to be automatically loaded when booting. Take the compiled .dtbo file, and copy to boot/overlays

	sudo cp spi_disabler.dtbo /boot/overlays/spi_disabler.dtbo

Add a line to /boot/config.txt:

	dtoverlay=spi_disabler

The kernel module itself can be loaded when booting by doing the following:

Check the active kernel version:

    uname -r

Copy the compiled rs485_ch.ko module to /lib/modules/<version>

    sudo cp rs485_ch.ko /lib/modules/`uname -r`/rs485_ch.ko

Next, run:

    sudo depmod -a

Finally, verify that the module can be loaded:

    sudo modprobe rs485_ch

To have the module load when booting, add the following to /etc/modules,
append your module to the end of the list, sans file extension. The SPI driver
must be loaded first, as the rs485_ch driver depends on it. Mine looks like:

    # /etc/modules: kernel modules to load at boot time.
    #
    # This file contains the names of kernel modules that should be loaded
    # at boot time, one per line. Lines beginning with "#" are ignored.
    # Parameters can be specified after the module name.

    spi-bcm2835
    rs485_ch

Reboot!

### Who do I talk to? ###

* Eirik Taylor at uzzors2k.com
