EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "High Speed RS-485 Raspberry Pi Interface"
Date "2020-08-22"
Rev "A"
Comp "uzzors2k.com"
Comment1 "Designed by Eirik Taylor"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L kicad_lib_uzzors2k:XR20M1170_26p_TSSOP U1
U 1 1 5F39B1FA
P 5500 3800
F 0 "U1" H 5700 5000 50  0000 C CNN
F 1 "XR20M1170_26p_TSSOP" H 6100 4900 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 6450 5100 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/146/XR20M1170-1889071.pdf" H 5350 4300 50  0001 C CNN
F 4 "701-XR20M1170IG16-F " H 6100 5000 50  0001 C CNN "Mouser"
	1    5500 3800
	1    0    0    -1  
$EndComp
$Comp
L kicad_lib_uzzors2k:THVD1550 U2
U 1 1 5F39C81F
P 8800 3300
F 0 "U2" H 8350 3850 50  0000 C CNN
F 1 "THVD1550" H 8500 3750 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9700 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/thvd1552.pdf" H 8800 3300 50  0001 C CNN
F 4 "595-THVD1550DR" H 9250 3850 50  0001 C CNN "Mouser"
	1    8800 3300
	1    0    0    -1  
$EndComp
Text Notes 1250 5700 0    50   ~ 0
NOTE: The wiring has not changed between the\n B+, A+, Pi2, Pi3, or Pi0.
$Comp
L Oscillator:XO32 X1
U 1 1 5F39DC77
P 3700 6800
F 0 "X1" H 4044 6846 50  0000 L CNN
F 1 "XO32" H 4044 6755 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_EuroQuartz_XO91-4Pin_7.0x5.0mm_HandSoldering" H 4400 6450 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/B400/XO32.pdf" H 3600 6800 50  0001 C CNN
F 4 "774-CB3LV-3I-64M0000" H 3700 6800 50  0001 C CNN "Mouser"
	1    3700 6800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 5F39F0E3
P 5500 2050
F 0 "#PWR0101" H 5500 1900 50  0001 C CNN
F 1 "+3.3V" H 5515 2223 50  0000 C CNN
F 2 "" H 5500 2050 50  0001 C CNN
F 3 "" H 5500 2050 50  0001 C CNN
	1    5500 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5F39F74E
P 2450 2250
F 0 "#PWR0102" H 2450 2100 50  0001 C CNN
F 1 "+3.3V" H 2465 2423 50  0000 C CNN
F 2 "" H 2450 2250 50  0001 C CNN
F 3 "" H 2450 2250 50  0001 C CNN
	1    2450 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 5F39FADF
P 2150 2250
F 0 "#PWR0103" H 2150 2100 50  0001 C CNN
F 1 "+5V" H 2165 2423 50  0000 C CNN
F 2 "" H 2150 2250 50  0001 C CNN
F 3 "" H 2150 2250 50  0001 C CNN
	1    2150 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5F3A011A
P 8800 2800
F 0 "#PWR0104" H 8800 2650 50  0001 C CNN
F 1 "+5V" H 8815 2973 50  0000 C CNN
F 2 "" H 8800 2800 50  0001 C CNN
F 3 "" H 8800 2800 50  0001 C CNN
	1    8800 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5F3A07B8
P 2050 5200
F 0 "#PWR0105" H 2050 4950 50  0001 C CNN
F 1 "GND" H 2055 5027 50  0000 C CNN
F 2 "" H 2050 5200 50  0001 C CNN
F 3 "" H 2050 5200 50  0001 C CNN
	1    2050 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F3A0D68
P 5500 5050
F 0 "#PWR0106" H 5500 4800 50  0001 C CNN
F 1 "GND" H 5505 4877 50  0000 C CNN
F 2 "" H 5500 5050 50  0001 C CNN
F 3 "" H 5500 5050 50  0001 C CNN
	1    5500 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F3A110A
P 3700 7100
F 0 "#PWR0107" H 3700 6850 50  0001 C CNN
F 1 "GND" H 3705 6927 50  0000 C CNN
F 2 "" H 3700 7100 50  0001 C CNN
F 3 "" H 3700 7100 50  0001 C CNN
	1    3700 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F3A16A3
P 8800 4000
F 0 "#PWR0108" H 8800 3750 50  0001 C CNN
F 1 "GND" H 8805 3827 50  0000 C CNN
F 2 "" H 8800 4000 50  0001 C CNN
F 3 "" H 8800 4000 50  0001 C CNN
	1    8800 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0109
U 1 1 5F3A1B33
P 3700 6200
F 0 "#PWR0109" H 3700 6050 50  0001 C CNN
F 1 "+3.3V" H 3715 6373 50  0000 C CNN
F 2 "" H 3700 6200 50  0001 C CNN
F 3 "" H 3700 6200 50  0001 C CNN
	1    3700 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5F3A2F05
P 4300 6400
F 0 "C1" H 4415 6446 50  0000 L CNN
F 1 "100nF" H 4415 6355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4338 6250 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/445/885012207098-1727748.pdf" H 4300 6400 50  0001 C CNN
F 4 "710-885012207098" H 4300 6400 50  0001 C CNN "Mouser"
	1    4300 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5F3A374D
P 10200 3550
F 0 "R8" H 10270 3596 50  0000 L CNN
F 1 "120R" H 10270 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 10130 3550 50  0001 C CNN
F 3 "~" H 10200 3550 50  0001 C CNN
F 4 "660-RK73B2ETTD121J" H 10200 3550 50  0001 C CNN "Mouser"
	1    10200 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 5F3A40BB
P 10700 3750
F 0 "J5" H 10850 3400 50  0000 R CNN
F 1 "RS-485" H 10850 3500 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10700 3750 50  0001 C CNN
F 3 "~" H 10700 3750 50  0001 C CNN
	1    10700 3750
	-1   0    0    1   
$EndComp
$Comp
L kicad_lib_uzzors2k:ScopeProbe TP6
U 1 1 5F3A4F4B
P 4950 6800
F 0 "TP6" H 5428 6796 50  0000 L CNN
F 1 "ScopeProbe" H 5428 6705 50  0000 L CNN
F 2 "uzzors2k:Probe_Hole" H 4950 6650 50  0001 C CNN
F 3 "" H 4950 6650 50  0001 C CNN
	1    4950 6800
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5F3A5EC3
P 2950 6600
F 0 "TP2" H 3008 6718 50  0000 L CNN
F 1 "TestPoint" H 3008 6627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 3150 6600 50  0001 C CNN
F 3 "~" H 3150 6600 50  0001 C CNN
	1    2950 6600
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0110
U 1 1 5F3AC812
P 2600 6200
F 0 "#PWR0110" H 2600 6050 50  0001 C CNN
F 1 "+3.3V" H 2615 6373 50  0000 C CNN
F 2 "" H 2600 6200 50  0001 C CNN
F 3 "" H 2600 6200 50  0001 C CNN
	1    2600 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F3ACD95
P 2600 6550
F 0 "R1" H 2670 6596 50  0000 L CNN
F 1 "10k" H 2670 6505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2530 6550 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/54/Bourns_CMP_Datasheet_05.28.20-1854233.pdf" H 2600 6550 50  0001 C CNN
F 4 "652-CMP0805-FX-1002L" H 2600 6550 50  0001 C CNN "Mouser"
	1    2600 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6800 2950 6800
Wire Wire Line
	2600 6800 2600 6700
Wire Wire Line
	2600 6200 2600 6400
Wire Wire Line
	2950 6600 2950 6800
Connection ~ 2950 6800
Wire Wire Line
	2950 6800 2600 6800
Wire Wire Line
	3700 6200 3700 6250
Wire Wire Line
	4300 6250 3700 6250
Connection ~ 3700 6250
Wire Wire Line
	3700 6250 3700 6500
$Comp
L power:GND #PWR0111
U 1 1 5F3B1EC4
P 4300 6550
F 0 "#PWR0111" H 4300 6300 50  0001 C CNN
F 1 "GND" H 4305 6377 50  0000 C CNN
F 2 "" H 4300 6550 50  0001 C CNN
F 3 "" H 4300 6550 50  0001 C CNN
	1    4300 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6800 4750 6800
$Comp
L power:GND #PWR0112
U 1 1 5F3B259B
P 4950 7100
F 0 "#PWR0112" H 4950 6850 50  0001 C CNN
F 1 "GND" H 4955 6927 50  0000 C CNN
F 2 "" H 4950 7100 50  0001 C CNN
F 3 "" H 4950 7100 50  0001 C CNN
	1    4950 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 6950 4950 7100
Wire Wire Line
	4700 4400 4450 4400
Wire Wire Line
	4450 4400 4450 5900
Wire Wire Line
	4450 5900 4750 5900
Wire Wire Line
	4750 5900 4750 6800
Connection ~ 4750 6800
Wire Wire Line
	4750 6800 4950 6800
$Comp
L power:GND #PWR0113
U 1 1 5F3B3958
P 5000 2400
F 0 "#PWR0113" H 5000 2150 50  0001 C CNN
F 1 "GND" H 5005 2227 50  0000 C CNN
F 2 "" H 5000 2400 50  0001 C CNN
F 3 "" H 5000 2400 50  0001 C CNN
	1    5000 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2050 5500 2100
Wire Wire Line
	5000 2100 5500 2100
Connection ~ 5500 2100
Wire Wire Line
	5500 2100 5500 2650
Wire Wire Line
	2150 2250 2150 2300
Wire Wire Line
	2150 2300 2250 2300
Wire Wire Line
	2250 2300 2250 2400
Connection ~ 2150 2300
Wire Wire Line
	2150 2300 2150 2400
Wire Wire Line
	2450 2250 2450 2300
Wire Wire Line
	2450 2300 2550 2300
Wire Wire Line
	2550 2300 2550 2400
Connection ~ 2450 2300
Wire Wire Line
	2450 2300 2450 2400
Wire Wire Line
	2050 5000 2050 5050
Wire Wire Line
	1950 5000 1950 5050
Wire Wire Line
	1950 5050 2050 5050
Connection ~ 2050 5050
Wire Wire Line
	2050 5050 2050 5150
Wire Wire Line
	2150 5000 2150 5150
Wire Wire Line
	2150 5150 2050 5150
Connection ~ 2050 5150
Wire Wire Line
	2050 5200 2050 5150
$Comp
L power:GND #PWR0114
U 1 1 5F3B915E
P 2350 5200
F 0 "#PWR0114" H 2350 4950 50  0001 C CNN
F 1 "GND" H 2355 5027 50  0000 C CNN
F 2 "" H 2350 5200 50  0001 C CNN
F 3 "" H 2350 5200 50  0001 C CNN
	1    2350 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5F3B936F
P 2650 5200
F 0 "#PWR0115" H 2650 4950 50  0001 C CNN
F 1 "GND" H 2655 5027 50  0000 C CNN
F 2 "" H 2650 5200 50  0001 C CNN
F 3 "" H 2650 5200 50  0001 C CNN
	1    2650 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5000 2350 5050
Wire Wire Line
	2250 5000 2250 5050
Wire Wire Line
	2250 5050 2350 5050
Connection ~ 2350 5050
Wire Wire Line
	2350 5050 2350 5150
Wire Wire Line
	2450 5000 2450 5150
Wire Wire Line
	2450 5150 2350 5150
Connection ~ 2350 5150
Wire Wire Line
	2350 5150 2350 5200
Wire Wire Line
	2650 5000 2650 5050
Wire Wire Line
	2550 5000 2550 5050
Wire Wire Line
	2550 5050 2650 5050
Connection ~ 2650 5050
Wire Wire Line
	2650 5050 2650 5200
$Comp
L Device:C C2
U 1 1 5F3BDEB4
P 5000 2250
F 0 "C2" H 5115 2296 50  0000 L CNN
F 1 "100nF" H 5115 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 2100 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/445/885012207098-1727748.pdf" H 5000 2250 50  0001 C CNN
F 4 "710-885012207098" H 5000 2250 50  0001 C CNN "Mouser"
	1    5000 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5F3BE3B5
P 8500 1950
F 0 "C3" H 8615 1996 50  0000 L CNN
F 1 "100nF" H 8615 1905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8538 1800 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/445/885012207098-1727748.pdf" H 8500 1950 50  0001 C CNN
F 4 "710-885012207098" H 8500 1950 50  0001 C CNN "Mouser"
	1    8500 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5F3BEAD0
P 9050 1950
F 0 "C4" H 9165 1996 50  0000 L CNN
F 1 "100nF" H 9165 1905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9088 1800 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/445/885012207098-1727748.pdf" H 9050 1950 50  0001 C CNN
F 4 "710-885012207098" H 9050 1950 50  0001 C CNN "Mouser"
	1    9050 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5F3C087B
P 8200 3850
F 0 "R5" H 8270 3896 50  0000 L CNN
F 1 "10k" H 8270 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8130 3850 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/54/Bourns_CMP_Datasheet_05.28.20-1854233.pdf" H 8200 3850 50  0001 C CNN
F 4 "652-CMP0805-FX-1002L" H 8200 3850 50  0001 C CNN "Mouser"
	1    8200 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5F3CD3E9
P 4500 4050
F 0 "#PWR0117" H 4500 3800 50  0001 C CNN
F 1 "GND" H 4505 3877 50  0000 C CNN
F 2 "" H 4500 4050 50  0001 C CNN
F 3 "" H 4500 4050 50  0001 C CNN
	1    4500 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3950 4500 3950
Wire Wire Line
	4500 3950 4500 4050
$Comp
L Connector:TestPoint TP5
U 1 1 5F3CEB90
P 4650 5100
F 0 "TP5" H 4592 5126 50  0000 R CNN
F 1 "TestPoint" H 4592 5217 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4850 5100 50  0001 C CNN
F 3 "~" H 4850 5100 50  0001 C CNN
	1    4650 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 4800 4650 4800
Wire Wire Line
	4650 4800 4650 5100
Wire Wire Line
	3700 3900 3700 3500
Wire Wire Line
	3800 4000 3800 3700
Wire Wire Line
	3900 3600 3900 4100
Wire Wire Line
	4000 4200 4000 3400
$Comp
L power:GND #PWR0118
U 1 1 5F3E8DDA
P 8200 4000
F 0 "#PWR0118" H 8200 3750 50  0001 C CNN
F 1 "GND" H 8205 3827 50  0000 C CNN
F 2 "" H 8200 4000 50  0001 C CNN
F 3 "" H 8200 4000 50  0001 C CNN
	1    8200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3900 8800 4000
Wire Wire Line
	8400 3400 8300 3400
Wire Wire Line
	8300 3400 8300 3300
Connection ~ 8300 3300
Wire Wire Line
	8300 3300 8400 3300
$Comp
L power:GND #PWR0119
U 1 1 5F3FB8BD
P 8500 2100
F 0 "#PWR0119" H 8500 1850 50  0001 C CNN
F 1 "GND" H 8505 1927 50  0000 C CNN
F 2 "" H 8500 2100 50  0001 C CNN
F 3 "" H 8500 2100 50  0001 C CNN
	1    8500 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5F3FBB8E
P 9050 2100
F 0 "#PWR0120" H 9050 1850 50  0001 C CNN
F 1 "GND" H 9055 1927 50  0000 C CNN
F 2 "" H 9050 2100 50  0001 C CNN
F 3 "" H 9050 2100 50  0001 C CNN
	1    9050 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0121
U 1 1 5F3FE600
P 8500 1800
F 0 "#PWR0121" H 8500 1650 50  0001 C CNN
F 1 "+5V" H 8515 1973 50  0000 C CNN
F 2 "" H 8500 1800 50  0001 C CNN
F 3 "" H 8500 1800 50  0001 C CNN
	1    8500 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0122
U 1 1 5F3FE800
P 9050 1800
F 0 "#PWR0122" H 9050 1650 50  0001 C CNN
F 1 "+5V" H 9065 1973 50  0000 C CNN
F 2 "" H 9050 1800 50  0001 C CNN
F 3 "" H 9050 1800 50  0001 C CNN
	1    9050 1800
	1    0    0    -1  
$EndComp
NoConn ~ 6400 3500
NoConn ~ 4700 3300
$Comp
L power:GND #PWR0123
U 1 1 5F416CAC
P 10500 3850
F 0 "#PWR0123" H 10500 3600 50  0001 C CNN
F 1 "GND" H 10505 3677 50  0000 C CNN
F 2 "" H 10500 3850 50  0001 C CNN
F 3 "" H 10500 3850 50  0001 C CNN
	1    10500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 3350 10500 3650
Text Notes 5900 5300 0    50   ~ 0
XR20M1170 supports up to 16Mbps with a\n64MHz crystal. THVD1550 supports up to 50Mbps\nwhen the bus length is short enough.
$Comp
L power:+3.3V #PWR0124
U 1 1 5F456A11
P 6750 2600
F 0 "#PWR0124" H 6750 2450 50  0001 C CNN
F 1 "+3.3V" H 6765 2773 50  0000 C CNN
F 2 "" H 6750 2600 50  0001 C CNN
F 3 "" H 6750 2600 50  0001 C CNN
	1    6750 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F45B243
P 6750 2750
F 0 "R4" H 6820 2796 50  0000 L CNN
F 1 "10k" H 6820 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6680 2750 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/54/Bourns_CMP_Datasheet_05.28.20-1854233.pdf" H 6750 2750 50  0001 C CNN
F 4 "652-CMP0805-FX-1002L" H 6750 2750 50  0001 C CNN "Mouser"
	1    6750 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D2
U 1 1 5F45C3EE
P 7200 3150
F 0 "D2" H 7200 2934 50  0000 C CNN
F 1 "1SS389,L3F" H 7200 3025 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-523" H 7200 3150 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/408/1SS389_datasheet_en_20140301-740935.pdf" H 7200 3150 50  0001 C CNN
F 4 "757-1SS389L3F" H 7200 3150 50  0001 C CNN "Mouser"
	1    7200 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	7050 3150 6750 3150
$Comp
L Device:D_TVS_x2_AAC D3
U 1 1 5F46DE51
P 9800 3400
F 0 "D3" V 9200 3350 50  0000 L CNN
F 1 "CDSOT23-SM712" V 9300 3350 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9650 3400 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/54/CDSOT23-SM712-1635169.pdf" H 9650 3400 50  0001 C CNN
F 4 "652-CDSOT23-SM712" H 9800 3400 50  0001 C CNN "Mouser"
	1    9800 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	10500 3750 10200 3750
Wire Wire Line
	10200 3750 10200 3700
Wire Wire Line
	10500 3350 10200 3350
Wire Wire Line
	10200 3350 10200 3400
$Comp
L power:GND #PWR0125
U 1 1 5F482AAE
P 9650 3400
F 0 "#PWR0125" H 9650 3150 50  0001 C CNN
F 1 "GND" H 9655 3227 50  0000 C CNN
F 2 "" H 9650 3400 50  0001 C CNN
F 3 "" H 9650 3400 50  0001 C CNN
	1    9650 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 3750 9800 3750
Connection ~ 10200 3750
Connection ~ 10200 3350
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5F3E7D7A
P 7500 2700
F 0 "J4" H 7550 2250 50  0000 R CNN
F 1 "UART" H 7650 2350 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7500 2700 50  0001 C CNN
F 3 "~" H 7500 2700 50  0001 C CNN
	1    7500 2700
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 5F3E94C1
P 7950 4000
F 0 "#PWR0126" H 7950 3750 50  0001 C CNN
F 1 "GND" H 7955 3827 50  0000 C CNN
F 2 "" H 7950 4000 50  0001 C CNN
F 3 "" H 7950 4000 50  0001 C CNN
	1    7950 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F3FA51D
P 4500 2400
F 0 "R2" H 4570 2446 50  0000 L CNN
F 1 "10k" H 4570 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4430 2400 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/54/Bourns_CMP_Datasheet_05.28.20-1854233.pdf" H 4500 2400 50  0001 C CNN
F 4 "652-CMP0805-FX-1002L" H 4500 2400 50  0001 C CNN "Mouser"
	1    4500 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0127
U 1 1 5F3FB0C9
P 4500 2050
F 0 "#PWR0127" H 4500 1900 50  0001 C CNN
F 1 "+3.3V" H 4515 2223 50  0000 C CNN
F 2 "" H 4500 2050 50  0001 C CNN
F 3 "" H 4500 2050 50  0001 C CNN
	1    4500 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5F3FFC4C
P 1700 6450
F 0 "TP1" H 1758 6568 50  0000 L CNN
F 1 "TestPoint" H 1758 6477 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 1900 6450 50  0001 C CNN
F 3 "~" H 1900 6450 50  0001 C CNN
	1    1700 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5F40012A
P 1700 6450
F 0 "#PWR0128" H 1700 6200 50  0001 C CNN
F 1 "GND" H 1705 6277 50  0000 C CNN
F 2 "" H 1700 6450 50  0001 C CNN
F 3 "" H 1700 6450 50  0001 C CNN
	1    1700 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5F400969
P 9450 3750
F 0 "R7" V 9550 3700 50  0000 C CNN
F 1 "10R" V 9650 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9380 3750 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/427/crcwhpe3-1713858.pdf" H 9450 3750 50  0001 C CNN
F 4 "71-CRCW080510R0FKEAH" H 9450 3750 50  0001 C CNN "Mouser"
	1    9450 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3750 9800 3750
Connection ~ 9800 3750
Wire Wire Line
	10200 2950 10200 3350
Wire Wire Line
	9800 3050 9800 2950
Wire Wire Line
	9800 2950 10200 2950
Wire Wire Line
	9600 2950 9800 2950
Connection ~ 9800 2950
Wire Wire Line
	9300 2950 9200 2950
Wire Wire Line
	9200 2950 9200 3150
Wire Wire Line
	9300 3750 9200 3750
Wire Wire Line
	9200 3750 9200 3550
Text Notes 9250 4300 0    50   ~ 0
ESD Protection. May or may not\nslow down the signal rate.
$Comp
L Device:LED D1
U 1 1 5F43014A
P 1100 5000
F 0 "D1" V 1139 4883 50  0000 R CNN
F 1 "Activity - RED" V 1048 4883 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1100 5000 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/239/Lite-On-LTST-S220KRKT-1175337.pdf" H 1100 5000 50  0001 C CNN
F 4 "859-LTST-S220KRKT" V 1100 5000 50  0001 C CNN "Mouser"
	1    1100 5000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5F43156D
P 1100 5250
F 0 "#PWR0129" H 1100 5000 50  0001 C CNN
F 1 "GND" H 1105 5077 50  0000 C CNN
F 2 "" H 1100 5250 50  0001 C CNN
F 3 "" H 1100 5250 50  0001 C CNN
	1    1100 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F431B34
P 1100 4650
F 0 "R3" H 1170 4696 50  0000 L CNN
F 1 "120R" H 1170 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1030 4650 50  0001 C CNN
F 3 "~" H 1100 4650 50  0001 C CNN
F 4 "652-CR0805FX-1200ELF" H 1100 4650 50  0001 C CNN "Mouser"
	1    1100 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 4800 1100 4850
Wire Wire Line
	1100 5150 1100 5250
$Comp
L Device:R R6
U 1 1 5F494F85
P 9450 2950
F 0 "R6" V 9243 2950 50  0000 C CNN
F 1 "10R" V 9334 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9380 2950 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/427/crcwhpe3-1713858.pdf" H 9450 2950 50  0001 C CNN
F 4 "71-CRCW080510R0FKEAH" H 9450 2950 50  0001 C CNN "Mouser"
	1    9450 2950
	0    1    1    0   
$EndComp
NoConn ~ 3150 2800
NoConn ~ 3150 2900
NoConn ~ 3150 3400
NoConn ~ 3150 3800
NoConn ~ 1550 3300
NoConn ~ 1550 3200
NoConn ~ 1550 2900
NoConn ~ 1550 2800
Wire Wire Line
	6400 2900 6550 2900
Wire Wire Line
	6550 3550 6550 2900
Wire Wire Line
	6550 3550 7850 3550
Wire Wire Line
	6750 2900 6750 3100
Wire Wire Line
	6400 3100 6750 3100
Connection ~ 6750 3100
Wire Wire Line
	6750 3100 6750 3150
Wire Wire Line
	7350 3150 8150 3150
Wire Wire Line
	6400 3300 8050 3300
Wire Wire Line
	8200 3700 8200 3300
Connection ~ 8200 3300
Wire Wire Line
	8200 3300 8300 3300
Wire Wire Line
	7700 2800 7950 2800
Wire Wire Line
	7950 2800 7950 4000
Wire Wire Line
	7700 2600 8050 2600
Wire Wire Line
	8050 2600 8050 3300
Connection ~ 8050 3300
Wire Wire Line
	8050 3300 8200 3300
Wire Wire Line
	7700 2500 8150 2500
Wire Wire Line
	8150 2500 8150 3150
Connection ~ 8150 3150
Wire Wire Line
	8150 3150 8400 3150
Wire Wire Line
	7700 2700 7850 2700
Wire Wire Line
	7850 2700 7850 3550
Connection ~ 7850 3550
Wire Wire Line
	7850 3550 8400 3550
Text Label 4150 3400 0    50   ~ 0
SCL
Text Label 4150 3500 0    50   ~ 0
~CS
Text Label 4150 3600 0    50   ~ 0
MOSI
Text Label 4150 3700 0    50   ~ 0
MISO
Text Label 4150 2850 0    50   ~ 0
~RESET
Text Label 4150 3000 0    50   ~ 0
~IRQ
Text Label 1100 4400 1    50   ~ 0
LED
Text Label 7500 3300 0    50   ~ 0
~RTS
Text Label 7500 3550 0    50   ~ 0
TX
Text Label 6850 3150 0    50   ~ 0
RX
NoConn ~ 1550 4000
NoConn ~ 1550 4100
NoConn ~ 1550 4200
NoConn ~ 1550 4400
NoConn ~ 3150 4400
Wire Wire Line
	3600 3600 3600 3000
Wire Wire Line
	3500 3500 3500 2850
$Comp
L power:+3.3V #PWR04
U 1 1 5F511787
P 3250 1450
F 0 "#PWR04" H 3250 1300 50  0001 C CNN
F 1 "+3.3V" H 3265 1623 50  0000 C CNN
F 2 "" H 3250 1450 50  0001 C CNN
F 3 "" H 3250 1450 50  0001 C CNN
	1    3250 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F511CD3
P 3050 2050
F 0 "#PWR03" H 3050 1800 50  0001 C CNN
F 1 "GND" H 3055 1877 50  0000 C CNN
F 2 "" H 3050 2050 50  0001 C CNN
F 3 "" H 3050 2050 50  0001 C CNN
	1    3050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1550 3250 1550
Wire Wire Line
	3250 1550 3250 1450
Wire Wire Line
	3050 1850 3050 2050
Wire Wire Line
	3250 3100 3150 3100
Wire Wire Line
	3150 3200 3350 3200
Wire Wire Line
	3350 3200 3350 1750
Wire Wire Line
	3050 1650 3250 1650
Wire Wire Line
	3050 1750 3350 1750
Wire Wire Line
	3250 1650 3250 3100
Text Label 3350 2600 1    50   ~ 0
I2C_SCL
Text Label 3250 2600 1    50   ~ 0
I2C_SDA
Wire Wire Line
	4500 2550 4500 2850
Wire Wire Line
	4500 2850 4700 2850
Wire Wire Line
	4500 2250 4500 2050
Text Label 4500 5900 0    50   ~ 0
64MHz
$Comp
L power:+5V #PWR05
U 1 1 5F5868C7
P 3150 1150
F 0 "#PWR05" H 3150 1000 50  0001 C CNN
F 1 "+5V" H 3165 1323 50  0000 C CNN
F 2 "" H 3150 1150 50  0001 C CNN
F 3 "" H 3150 1150 50  0001 C CNN
	1    3150 1150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 5F587066
P 2850 1650
F 0 "J1" H 2900 2100 50  0000 C CNN
F 1 "I2C" H 2900 2000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2850 1650 50  0001 C CNN
F 3 "~" H 2850 1650 50  0001 C CNN
	1    2850 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 1450 3150 1450
Wire Wire Line
	3150 1450 3150 1150
$Comp
L Device:C C5
U 1 1 5F595568
P 1900 1400
F 0 "C5" H 2015 1446 50  0000 L CNN
F 1 "100nF" H 2015 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1938 1250 50  0001 C CNN
F 3 "https://no.mouser.com/datasheet/2/445/885012207098-1727748.pdf" H 1900 1400 50  0001 C CNN
F 4 "710-885012207098" H 1900 1400 50  0001 C CNN "Mouser"
	1    1900 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5F59627E
P 1900 1550
F 0 "#PWR02" H 1900 1300 50  0001 C CNN
F 1 "GND" H 1905 1377 50  0000 C CNN
F 2 "" H 1900 1550 50  0001 C CNN
F 3 "" H 1900 1550 50  0001 C CNN
	1    1900 1550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5F596499
P 1900 1250
F 0 "#PWR01" H 1900 1100 50  0001 C CNN
F 1 "+5V" H 1915 1423 50  0000 C CNN
F 2 "" H 1900 1250 50  0001 C CNN
F 3 "" H 1900 1250 50  0001 C CNN
	1    1900 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3500 3500 3500
Wire Wire Line
	3150 3600 3600 3600
Wire Wire Line
	3150 3900 3700 3900
Wire Wire Line
	3150 4000 3800 4000
Wire Wire Line
	3150 4100 3900 4100
Wire Wire Line
	3150 4200 4000 4200
$Comp
L Connector:Conn_01x07_Male J3
U 1 1 5F5BF7B5
P 700 3100
F 0 "J3" H 750 3650 50  0000 C CNN
F 1 "GPIO" H 800 3550 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 700 3100 50  0001 C CNN
F 3 "~" H 700 3100 50  0001 C CNN
	1    700  3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5F5C31D4
P 900 3400
F 0 "#PWR0116" H 900 3150 50  0001 C CNN
F 1 "GND" H 905 3227 50  0000 C CNN
F 2 "" H 900 3400 50  0001 C CNN
F 3 "" H 900 3400 50  0001 C CNN
	1    900  3400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Raspberry_Pi_2_3 J2
U 1 1 5F3916B5
P 2350 3700
F 0 "J2" H 1700 5000 50  0000 C CNN
F 1 "Raspberry_Pi_2_3_4_Zero" H 2000 5450 50  0000 C CNN
F 2 "uzzors2k:RaspberryPi_Zero" H 2350 3700 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 2350 3700 50  0001 C CNN
F 4 "485-2222" H 2350 3700 50  0001 C CNN "Mouser"
	1    2350 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4500 3150 5800
Wire Wire Line
	1450 2900 1450 3100
Wire Wire Line
	1450 3100 1550 3100
Wire Wire Line
	1400 3000 1400 3500
Wire Wire Line
	1400 3500 1550 3500
Wire Wire Line
	1100 3900 1100 4500
Wire Wire Line
	4500 2850 3500 2850
Connection ~ 4500 2850
Wire Wire Line
	4700 3000 3600 3000
Wire Wire Line
	4700 3400 4000 3400
Wire Wire Line
	4700 3500 3700 3500
Wire Wire Line
	4700 3600 3900 3600
Wire Wire Line
	4700 3700 3800 3700
Wire Wire Line
	900  2800 1000 2800
Wire Wire Line
	900  2900 1450 2900
Wire Wire Line
	900  3000 1400 3000
Wire Wire Line
	900  3100 1300 3100
Wire Wire Line
	900  3200 1200 3200
Wire Wire Line
	900  3300 1100 3300
Wire Wire Line
	1000 2800 1000 3750
Wire Wire Line
	850  5800 850  3750
Wire Wire Line
	850  5800 3150 5800
Wire Wire Line
	850  3750 1000 3750
Wire Wire Line
	1100 3300 1100 3700
Wire Wire Line
	1100 3700 1550 3700
Wire Wire Line
	1100 3900 1550 3900
Wire Wire Line
	1200 3200 1200 4300
Wire Wire Line
	1200 4300 1550 4300
Wire Wire Line
	1300 3100 1300 3600
Wire Wire Line
	1300 3600 1550 3600
$EndSCHEMATC
