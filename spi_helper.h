#ifndef _SPIHELPER_H_
#define _SPIHELPER_H_

#include <linux/module.h>
#include <linux/spi/spi.h>

MODULE_LICENSE("GPL");

int init_spiSlave(struct spi_device **spi_device, int bus_speed_hz);
void cleanup_Spi(struct spi_device *spi_device);

#endif
