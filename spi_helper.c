#include "spi_helper.h"

#include <linux/spi/spi.h>

int init_spiSlave(struct spi_device **spi_device, int bus_speed_hz)
{
    int ret;
    struct spi_master *master;

    //Register information about your slave device:
    struct spi_board_info spi_device_info = {
        .modalias = "XR20M1170",
        .max_speed_hz = bus_speed_hz,	//speed your device (slave) can handle
        .bus_num = 0,
        .chip_select = 0,
        .mode = 0,	// Clock idles low. Data sampled at first clock edge.
    };

    // To send data we have to know what spi port/pins should be used.
    // This information can be found in the device-tree.
    master = spi_busnum_to_master(spi_device_info.bus_num);
    if (!master) {
        pr_alert("RS485: SPI MASTER not found. Has it been enabled under raspi-config?\n");
        return -ENODEV;
    }

    // create a new slave device, given the master and device info
    *spi_device = spi_new_device(master, &spi_device_info);

    if (!(*spi_device)) {
        pr_alert("RS485: FAILED to create SPI slave.\n");
        return -ENODEV;
    }

    (*spi_device)->bits_per_word = 8;
    ret = spi_setup(*spi_device);

    if (ret) {
        pr_alert("RS485: FAILED to setup SPI slave.\n");
        spi_unregister_device(*spi_device);
        return -ENODEV;
    }

    return 0;
}

void cleanup_Spi(struct spi_device *spi_device)
{
    if (spi_device) {
        spi_unregister_device(spi_device);
    }
}
