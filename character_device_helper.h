#ifndef _CHARDEVHELPER_H_
#define _CHARDEVHELPER_H_

#include <linux/module.h>
#include <linux/cdev.h>

MODULE_LICENSE("GPL");

#define DEVICE_NAME "rs485_ch"

int chardev_uevent_callback(struct device *dev, struct kobj_uevent_env *env);
int initialize_character_device_data(const struct file_operations * fileops);
void finalize_character_device_data(void);

#endif
