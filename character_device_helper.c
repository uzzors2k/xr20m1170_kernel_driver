#include "character_device_helper.h"

#include <linux/fs.h>
#include <linux/cdev.h>

struct character_device_data {
    struct cdev cdev;
    struct class *pclass;
    dev_t device_ID;
};

static struct character_device_data character_device;

int chardev_uevent_callback(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

int initialize_character_device_data(const struct file_operations * fileops)
{
    int err;

    // allocate chardev region and assign Major number
    err = alloc_chrdev_region(&(character_device.device_ID), 0, 1, DEVICE_NAME);
    if (err) {
        pr_alert("RS485: Can't alloc minor\n");
        return -ENODEV;
    }

    // init new device
    cdev_init(&character_device.cdev, fileops);
    character_device.cdev.owner = THIS_MODULE;

    // add device to the system where "i" is a Minor number of the new device
    err = cdev_add(&character_device.cdev, character_device.device_ID, 1);
    if (err) {
        pr_alert("RS485: cdev_add() failed\n");
        unregister_chrdev_region(character_device.device_ID, 1);
        return -ENODEV;
    }

    // create sysfs class
    character_device.pclass = class_create(THIS_MODULE, DEVICE_NAME);
    character_device.pclass->dev_uevent = chardev_uevent_callback;

    // create device node /dev/<DEVICE_NAME>
    if (IS_ERR(device_create(character_device.pclass, NULL, character_device.device_ID, NULL, DEVICE_NAME))) {
        pr_alert("RS485: device_create() failed\n");
        class_destroy(character_device.pclass);
        cdev_del(&character_device.cdev);
        unregister_chrdev_region(character_device.device_ID, 1);
        return -ENOMEM;
    }

    return 0;
}

void finalize_character_device_data(void)
{
    device_destroy(character_device.pclass, character_device.device_ID);
    class_destroy(character_device.pclass);

    cdev_del(&character_device.cdev);
    unregister_chrdev_region(character_device.device_ID, 1);
}
