obj-m += rs485_ch.o
rs485_ch-objs := rs485.o xr20m1170_lib.o spi_helper.o gpio_helper.o character_device_helper.o

# Current dir
PWD := $(shell pwd)

# Kernel sources
KDIR := ../../linux
KDIR2 := ../../linux_v2

# Build targets. Only difference is the KDIR.
rp1:
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules -C $(KDIR) M=$(PWD) modules

rpz:
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules -C $(KDIR) M=$(PWD) modules

rp2:
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules -C $(KDIR2) M=$(PWD) modules

rp3:
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules -C $(KDIR2) M=$(PWD) modules

clean:
	make -C $(KDIR) M=$(PWD) clean
