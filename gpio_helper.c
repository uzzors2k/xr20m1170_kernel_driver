#include "gpio_helper.h"

#include <linux/gpio.h>
#include <linux/interrupt.h>

static unsigned int irqNumber;

int init_gpioControl(struct gpio gpioControl[], size_t num,
     irq_handler_t interrupt_handler, int irq_index)
{
    // Setup GPIO control
    int result;
    result = gpio_request_array(gpioControl, num);
    if (result < 0) {
        pr_alert("RS485: Failed to initialize GPIO");
        return -ENODEV;
    }

    irqNumber = gpio_to_irq(gpioControl[irq_index].gpio);

    // This next call requests an interrupt line
    result = request_irq(irqNumber,             // The interrupt number requested
        interrupt_handler,                      // The pointer to the handler function below
        IRQF_TRIGGER_FALLING,                   // Interrupt on rising edge (button press, not release)
        "XR20M1170_irq",                        // Used in /proc/interrupts to identify the owner
        NULL);                                  // The *dev_id for shared interrupt lines, NULL is okay

    if (result < 0) {
    	pr_alert("RS485: Failed to acquire irq");
    	return -ENODEV;
    }

    return 0;
}

void cleanup_Gpio(struct gpio gpioControl[], size_t num)
{
    size_t i;

    free_irq(irqNumber, NULL);               // Free the IRQ number, no *dev_id required in this case

    for (i=0; i < num; i++) {
        gpio_set_value(gpioControl[i].gpio, 0);
    }

    gpio_free_array(gpioControl, num);
}
