#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/wait.h>
#include <linux/completion.h>
#include <linux/mutex.h>
#include <linux/semaphore.h>
#include <linux/ioctl.h>

#include "xr20m1170_lib.h"
#include "spi_helper.h"
#include "gpio_helper.h"
#include "character_device_helper.h"
#include "rs485_ioctl.h"


// TODO:    Calculation of DLD register value isn't precise, investigate. Will
//          cause marginally incorrect baudrates compared to the theorectial
//          best case.


// Designed by Eirik Taylor, 10.01.2021 - 07.11.2021

// NOTE:
// Only works if SPIdev is blocked, and SPI is enabled from raspi-config.
// Functions with full 64 byte reads and writes to the HW buffers of the xr20m1170

// SPI Documentation
// https://www.kernel.org/doc/html/v4.14/driver-api/spi.html

// GPIO Documentation
// https://docs.huihoo.com/doxygen/linux/kernel/3.7/include_2linux_2gpio_8h.html

// General Kernel Module programming example
// https://olegkutkov.me/2018/03/14/simple-linux-character-device-driver/

// With fixes from
// https://stackoverflow.com/questions/29392269/linux-my-char-driver-creates-a-node-in-sysfs-but-can-not-clean-it-after-reloadi
// Had errors when unloading the device, so the registration code was changed.

// GPIO interrupts
// http://derekmolloy.ie/kernel-gpio-programming-buttons-and-leds/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Eirik Taylor <uzzors2k@gmail.com>");
MODULE_DESCRIPTION("XR20M1170 serial kernel module, for RS-485 interface. Works on SPI0.");
MODULE_VERSION("0.7.1");

// Hardware devices //
static irq_handler_t uart_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);

#define LED_INDEX 0
#define RESET_INDEX 1
#define IRQ_INDEX 2
static struct gpio gpioControl[] = {
    { 22, (GPIOF_DIR_OUT | GPIOF_INIT_LOW), "REDLED" },
    { 5, (GPIOF_DIR_OUT | GPIOF_INIT_HIGH), "_RESET" },
    { 6, (GPIOF_DIR_IN | GPIOF_INIT_HIGH), "_IRQ" }
};

static struct spi_device *spi_device;

////////////////////////

// Control system //

static int keepRunning = 1;
static struct task_struct *controller_task;
static struct completion controller_task_done;

static struct mutex transmit_buffer_mutex;
static struct mutex receive_buffer_mutex;
static struct semaphore unhandled_hw_interrupts;
static wait_queue_head_t rx_queue;
static wait_queue_head_t tx_queue;

static int controllerTask_Function(void *data);

////////////////////////

// Read/write buffer control //
#define HW_BUFFER_SIZE 64
#define SW_BUFFER_SIZE 512

static int txBufferDataLength = 0;
static int txBufferPendingHwTfxr = 0;
static uint8_t userToTxBuffer[HW_BUFFER_SIZE];

static uint8_t readDataBuffer[SW_BUFFER_SIZE + 1];
static int readBufferReadIndex = 0;
static int readBufferWriteIndex = 0;
////////////////////////

// Character Device Driver //

static ssize_t rs485chdev_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t rs485chdev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static long int rs485chdev_ioctl(struct file *file, unsigned int ioctl_num, long unsigned int ioctl_param);

static const struct file_operations rs485chdev_fops = {
    .owner      = THIS_MODULE,
    .read       = rs485chdev_read,
    .write      = rs485chdev_write,
    .unlocked_ioctl = rs485chdev_ioctl,
};

////////////////////////

// Parameters //

static int baudrate = 115200;           // Should support up to 16Mbaud over short range!
static int spiClockRate = 18000000;     // 18MHz

////////////////////////

static int __init rs485chdev_init(void)
{
    if (init_spiSlave(&spi_device, spiClockRate)) {
        return -ENODEV;
    }

    if (init_gpioControl(gpioControl, ARRAY_SIZE(gpioControl),
        (irq_handler_t) uart_irq_handler, IRQ_INDEX)) {
        return -ENODEV;
    }

    if (init_serial(spi_device, gpioControl[RESET_INDEX].gpio, baudrate)) {
        return -ENODEV;
    }

    if (initialize_character_device_data(&rs485chdev_fops)) {
        return -ENODEV;
    }

    mutex_init(&transmit_buffer_mutex);
    mutex_init(&receive_buffer_mutex);
    sema_init(&unhandled_hw_interrupts, 0);
    init_waitqueue_head(&rx_queue);
    init_waitqueue_head(&tx_queue);

    init_completion(&controller_task_done);
    controller_task = kthread_run(controllerTask_Function, NULL, "XR20M1170_control");
    if (IS_ERR(controller_task)) {
        complete(&controller_task_done);
        return PTR_ERR(controller_task);
    }

    printk("RS485: Init complete\n");

    return 0;
}

static void __exit rs485chdev_exit(void)
{
    keepRunning = 0;
    up(&unhandled_hw_interrupts);
    wait_for_completion(&controller_task_done);

    finalize_character_device_data();

    cleanup_Spi(spi_device);
    cleanup_Gpio(gpioControl, ARRAY_SIZE(gpioControl));

    printk("RS485: Exit complete\n");
}

static irq_handler_t uart_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
    //printk("RS485: UART IRQ Detected\n");

    up(&unhandled_hw_interrupts);
    return (irq_handler_t) IRQ_HANDLED;      // Announce that the IRQ has been handled correctly
}

static int controllerTask_Function(void *data)
{
    uint8_t readAdr = 0;
    uint8_t isrValue = ~INT_NONE;
    ssize_t spiReadResult;
    uint8_t remainingHwBytesCounter = 0;

    // Not sure why, but don't allow TX interrupts until here
    if (enable_tx_interrupt(spi_device, 1) < 0) {
        pr_err("RS485: FAILED to write to SPI device in initial setup of controller task!\n");
        return -EFAULT;
    }

    //printk("RS485: Starting controllerTask_Function\n");

    set_current_state(TASK_INTERRUPTIBLE);
    while (keepRunning != 0)
    {
        if (down_interruptible(&unhandled_hw_interrupts) != 0) {
            pr_alert("RS485: HW IRQ semaphore was awoken, no interrupt occured\n");
            continue;
        }

        ///// IRQ Handling //////
        //printk("RS485: Handling UART IRQ...\n");
        gpio_set_value(gpioControl[LED_INDEX].gpio, 1);

        // Interrupts are served up based on priority, from a queue.
        isrValue = ~INT_NONE;
        while (isrValue != INT_NONE) {
            // Check what the interrupt is for
            readAdr = READ_ADR | ISR_ADR;
            spiReadResult = spi_w8r8(spi_device, readAdr);
            if (spiReadResult < 0) {
                pr_err("RS485: FAILED to access SPI slave while reading ISR\n");
                gpio_set_value(gpioControl[LED_INDEX].gpio, 0);
                complete_and_exit(&controller_task_done, 0);
                return -1;
            }
            else {
                isrValue = ISR_MASK & (uint8_t)spiReadResult;
            }

            switch (isrValue) {
                case INT_RXRDY:
                    //printk("RS485: RXRDY (Received Data Ready)\n");
                    // fall through
                case INT_RX_TIMEOUT:
                {
                    //printk("RS485: RXRDY (Received Data Ready)\n");

                    spiReadResult = get_rx_fifo_level(spi_device);
                    if (spiReadResult < 0) {
                        pr_alert("RS485: FAILED to access SPI slave while reading RXLVL\n");
                        gpio_set_value(gpioControl[LED_INDEX].gpio, 0);
                        complete_and_exit(&controller_task_done, 0);
                        return -1;
                    }

                    remainingHwBytesCounter = (uint8_t)spiReadResult;
                    //printk("RS485: Bytes in rx buffer %d\n", remainingHwBytesCounter);

                    mutex_lock(&receive_buffer_mutex);
                    // Atomic section start //

                    while (remainingHwBytesCounter > 0)
                    {
                        if ((readBufferWriteIndex + remainingHwBytesCounter) >= SW_BUFFER_SIZE) {
                            // Buffer should reset when reading, overflow has occurred
                            readBufferReadIndex = 0;
                            readBufferWriteIndex = 0;
                            pr_warn("RS485: Read buffer overrun, data loss!\n");
                        }

                        // Attempt to read out the full buffer. Will return the actual number of bytes read
                        spiReadResult = read_rx_bytes(spi_device,
                            &readDataBuffer[readBufferWriteIndex],
                            remainingHwBytesCounter);

                        //printk("RS485: Received new RX buffer, first byte %d\n", readDataBuffer[readBufferWriteIndex]);

                        if (spiReadResult < 0) {
                            pr_err("RS485: FAILED to access SPI slave while reading RHR\n");
                            gpio_set_value(gpioControl[LED_INDEX].gpio, 0);
                            mutex_unlock(&receive_buffer_mutex);
                            complete_and_exit(&controller_task_done, 0);
                            return -1;
                        }
                        else {
                            remainingHwBytesCounter -= (uint8_t)spiReadResult;
                            readBufferWriteIndex += (uint8_t)spiReadResult;
                            //printk("RS485: Read data, remaining bytes in rx buffer %d\n", remainingHwBytesCounter);
                        }
                    }

                    // Atomic section end //
                    mutex_unlock(&receive_buffer_mutex);

                    wake_up_interruptible_all(&rx_queue);
                    break;
                }

                case INT_TXRDY:
                {
                    //printk("RS485: TXRDY (Transmit Ready)\n");

                    // Check if the TX FIFO is ready for more data. This interrupt
                    // is trigger once we are down to a number of spaces in the TX FIFO,
                    // however given the speed things operate at, we can easily add more
                    // data before the transmission is complete.
                    // The interrupt also appears to trigger once the buffer is completely empty
                    // So twice per transmission

                    // Wait until the buffer is completely empty
                    remainingHwBytesCounter = 0;
                    do {
                        readAdr = READ_ADR | LSR_ADR;
                        spiReadResult = spi_w8r8(spi_device, readAdr);
                        if (spiReadResult < 0) {
                            pr_alert("RS485: FAILED to access SPI slave while reading TXLVL\n");
                            gpio_set_value(gpioControl[LED_INDEX].gpio, 0);
                            complete_and_exit(&controller_task_done, 0);
                            return -1;
                        }

                        // Mask out the Transmit Holding Register Empty Flag
                        remainingHwBytesCounter = LSR_TX_THE & (uint8_t)spiReadResult;

                    } while(remainingHwBytesCounter == 0);

                    mutex_lock(&transmit_buffer_mutex);
                    // Atomic section start //
                    if (txBufferPendingHwTfxr == 0) {
                        //printk("RS485: Entire TX buffer sent!\n");
                        txBufferDataLength = 0;
                    }
                    // Atomic section end //
                    mutex_unlock(&transmit_buffer_mutex);
                    wake_up_interruptible_all(&tx_queue);

                    break;
                }

                case INT_LSR:
                {
                    //printk("RS485: LSR (Receiver Line Status Register)\n");
                    break;
                }

                case INT_MSR:
                {
                    //printk("RS485: MSR (Modem Status Register)\n");
                    break;
                }

                case INT_GPIO:
                {
                    //printk("RS485: GPIO (General Purpose Inputs)\n");
                    break;
                }

                case INT_SPL_CHR:
                {
                    //printk("RS485: RXRDY (Received Xoff or Special character)\n");
                    break;
                }

                case INT_HW_FLW:
                {
                    //printk("RS485: CTS#, RTS# change of state\n");
                    break;
                }

                case INT_NONE:
                {
                    //printk("RS485: -- All ISRs handled\n");
                    break;
                }

                default:
                {
                    pr_alert("RS485: Unsupported IRQ from XR20M1170, %d\n", isrValue);
                    complete_and_exit(&controller_task_done, 0);
                    return -1;
                }
            }
        }

        gpio_set_value(gpioControl[LED_INDEX].gpio, 0);
        //printk("RS485: Handled UART IRQ\n");

        mutex_lock(&transmit_buffer_mutex);
        // Atomic section start //
        if ((txBufferDataLength != 0) && (txBufferPendingHwTfxr != 0)) {
            //printk("RS485: Loading new TX buffer, first byte %d\n", userToTxBuffer[0]);
            spiReadResult = serial_transmit_bytes(spi_device, userToTxBuffer, txBufferDataLength);
            txBufferPendingHwTfxr = 0;

            // Atomic section end //
            mutex_unlock(&transmit_buffer_mutex);

            if (spiReadResult < 0) {
                pr_err("RS485: FAILED to access SPI slave when attempting to transmit data\n");
                gpio_set_value(gpioControl[LED_INDEX].gpio, 0);
                complete_and_exit(&controller_task_done, 0);
                return -1;
            }
        }
        // Atomic section end //
        mutex_unlock(&transmit_buffer_mutex);

        set_current_state(TASK_INTERRUPTIBLE);
        ////////

    }

    printk("RS485: Exiting controllerTask_Function\n");
    complete_and_exit(&controller_task_done, 0);
    return 0;
}

static ssize_t rs485chdev_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    size_t datalen;

    //printk("RS485: Read file->f_flags: %d\n", file->f_flags);

    mutex_lock(&receive_buffer_mutex);
    // Atomic section start //

    if (readBufferReadIndex == readBufferWriteIndex) {
        //printk("RS485: No data ready to read!\n");
        mutex_unlock(&receive_buffer_mutex);

        if (file->f_flags & MAY_NOT_BLOCK) {
            return -EAGAIN;
        }

        // Wait for a change in the rx buffer
        wait_event_interruptible(rx_queue, (readBufferReadIndex != readBufferWriteIndex));
        mutex_lock(&receive_buffer_mutex);
    }

    datalen = readBufferWriteIndex - readBufferReadIndex;

    if (count > datalen) {
        count = datalen;
    }

    if (copy_to_user(buf, readDataBuffer + readBufferReadIndex, count)) {
        pr_err("RS485: Unable to data to user buffer!\n");
        mutex_unlock(&receive_buffer_mutex);
        return -EFAULT;
    }

    readBufferReadIndex += count;
    if (readBufferReadIndex == readBufferWriteIndex) {
        // All data has been read, move the memory pointers back to zero
        readBufferReadIndex = 0;
        readBufferWriteIndex = 0;
        //printk("RS485: Resetting read buffer to zero.\n");
    }

    // Atomic section end //
    mutex_unlock(&receive_buffer_mutex);

    return count;
}

static ssize_t rs485chdev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    size_t copyCount = HW_BUFFER_SIZE;
    size_t notCopied;

    //printk("RS485: Write file->f_flags: %d\n", file->f_flags);

    mutex_lock(&transmit_buffer_mutex);
    // Atomic section start //

    if (txBufferDataLength != 0) {
        //printk("RS485: Transmission in progress already\n");
        mutex_unlock(&transmit_buffer_mutex);

        if (file->f_flags & MAY_NOT_BLOCK) {
            return -EAGAIN;
        }

        // Wait for the transmission to complete
        wait_event_interruptible(tx_queue, (txBufferDataLength == 0));
        mutex_lock(&transmit_buffer_mutex);
    }

    if (count < copyCount) {
        copyCount = count;
    }

    notCopied = copy_from_user(userToTxBuffer, buf, copyCount);

    if (notCopied == 0) {
        //printk("RS485: Copied %zd bytes from the user\n", copyCount);
    } else {
        //printk("RS485: Could't copy %zd bytes from the user\n", notCopied);
    }

    // Set the length of data to send from the buffer, and also indicate that data is ready
    txBufferDataLength = copyCount;
    txBufferPendingHwTfxr = 1;

    // Atomic section end //
    mutex_unlock(&transmit_buffer_mutex);

    // Wake up the control thread, so the full TX buffer is noticed
    up(&unhandled_hw_interrupts);

    return copyCount;
}

static long int rs485chdev_ioctl(struct file *file, unsigned int ioctl_num, long unsigned int ioctl_param)
{
    switch (ioctl_num) {
        case RS485IOC_SETSPICLK:
            if (copy_from_user(&spiClockRate, (int32_t*)ioctl_param, sizeof(spiClockRate))) {
                pr_err("RS485: Error copying data from userspace when setting SPI clock!\n");
                return -EFAULT;
            }

            printk("RS485: ioctl %u (RS485IOC_SETSPICLK), setting SPI clock rate to %d from the user\n", ioctl_num, spiClockRate);

            cleanup_Spi(spi_device);

            if (init_spiSlave(&spi_device, spiClockRate)) {
                pr_err("RS485: Error when attempting to reinitialize the SPI device!\n");
                return -ENODEV;
            }

            break;

        case RS485IOC_SETBAUD:
            if (copy_from_user(&baudrate, (int32_t*)ioctl_param, sizeof(baudrate))) {
                pr_err("RS485: Error copying data from userspace when setting baudrate!\n");
                return -EFAULT;
            }

            printk("RS485: ioctl %u (RS485IOC_SETBAUD), setting baudrate to %d from the user\n", ioctl_num, baudrate);

            if (init_serial(spi_device, gpioControl[RESET_INDEX].gpio, baudrate)) {
                pr_err("RS485: Error when attempting set baudrate!\n");
                return -EFAULT;
            }

            if (enable_tx_interrupt(spi_device, 1) < 0) {
                pr_err("RS485: Error when attempting set enable interrupts after setting baudrate!\n");
                return -EFAULT;
            }
            break;

            case RS485IOC_FLUSH:
                printk("RS485: ioctl %u (RS485IOC_FLUSH), clearing all internal buffers\n", ioctl_num);

                // Should perhaps ensure there are no pending long term IO operations on-going,
                // but on the other hand this is up to the caller to handle.
                mutex_lock(&transmit_buffer_mutex);
                mutex_lock(&receive_buffer_mutex);

                txBufferDataLength = 0;
                readBufferReadIndex = 0;
                readBufferWriteIndex = 0;

                mutex_unlock(&receive_buffer_mutex);
                mutex_unlock(&transmit_buffer_mutex);

                // Do the below to reset the device, and clear internal buffers
                if (init_serial(spi_device, gpioControl[RESET_INDEX].gpio, baudrate)) {
                    pr_err("RS485: Error when attempting set baudrate!\n");
                    return -EFAULT;
                }

                if (enable_tx_interrupt(spi_device, 1) < 0) {
                    pr_err("RS485: Error when attempting set enable interrupts after setting baudrate!\n");
                    return -EFAULT;
                }

                break;

        default:
            pr_alert("RS485: Invalid ioctl request! Index %u \n", ioctl_num);
            return -1;
    }
    return 0;
}


module_init(rs485chdev_init);
module_exit(rs485chdev_exit);
