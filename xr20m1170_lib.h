#ifndef _XR20M1170LIB_H_
#define _XR20M1170LIB_H_

#include <linux/module.h>
#include <linux/spi/spi.h>

MODULE_LICENSE("GPL");

// XR20M1170 Registers

// Command Registers
#define READ_ADR 0b10000000
#define WRITE_ADR 0b00000000

// Address space spans bits 6:3
#define INTERNAL_ADR_OFFSET 3

// 16C550 Compatible Registers
#define RHR_ADR (0x00 << INTERNAL_ADR_OFFSET)
#define THR_ADR (0x00 << INTERNAL_ADR_OFFSET)
#define IER_ADR (0x01 << INTERNAL_ADR_OFFSET)
#define ISR_ADR (0x02 << INTERNAL_ADR_OFFSET)
#define FCR_ADR (0x02 << INTERNAL_ADR_OFFSET)
#define LCR_ADR (0x03 << INTERNAL_ADR_OFFSET)
#define MCR_ADR (0x04 << INTERNAL_ADR_OFFSET)
#define LSR_ADR (0x05 << INTERNAL_ADR_OFFSET)
#define MSR_ADR (0x06 << INTERNAL_ADR_OFFSET)
#define SPR_ADR (0x07 << INTERNAL_ADR_OFFSET)
#define TXLVL_ADR (0x08 << INTERNAL_ADR_OFFSET)
#define RXLVL_ADR (0x09 << INTERNAL_ADR_OFFSET)
#define EFCR_ADR (0x0F << INTERNAL_ADR_OFFSET)
// Baud Rate Generator Divisor
#define DLL_ADR (0x00 << INTERNAL_ADR_OFFSET)
#define DLM_ADR (0x01 << INTERNAL_ADR_OFFSET)
#define DLD_ADR (0x02 << INTERNAL_ADR_OFFSET)
// Enhanced Registers
#define EFR_ADR (0x02 << INTERNAL_ADR_OFFSET)
#define TCR_ADR (0x06 << INTERNAL_ADR_OFFSET)
#define TLR_ADR (0x07 << INTERNAL_ADR_OFFSET)

// ISR Register bits
#define ISR_MASK 0b00111111         // Bits 7 and 6 are to indicate FIFO active
#define INT_LSR 0b000110            // LSR (Receiver Line Status Register)
#define INT_RX_TIMEOUT 0b001100     // RXRDY (Receive Data Time-out)
#define INT_RXRDY 0b000100          // RXRDY (Received Data Ready)
#define INT_TXRDY 0b000010          // TXRDY (Transmit Ready)
#define INT_MSR 0b000000            // MSR (Modem Status Register)
#define INT_GPIO 0b110000           // GPIO (General Purpose Inputs)
#define INT_SPL_CHR 0b010000        // RXRDY (Received Xoff or Special character)
#define INT_HW_FLW 0b100000         // CTS#, RTS# change of state
#define INT_NONE 0b000001           // None (default)

// IER Register bits
#define IER_RHR (1 << 0)            // Interrupt on received data
#define IER_THR (1 << 1)            // Interrupt on transmitter empty

// FCR Register bits
#define FCR_FIFO_EN (1 << 0)        // TX and RX FIFO Enable
#define FCR_RX_RST (1 << 1)         // RX FIFO Reset
#define FCR_TX_RST (1 << 2)         // TX FIFO Reset
#define FCR_TX_OFFSET 4             // Address space spans bits 5:4
#define FCR_RX_OFFSET 6             // Address space spans bits 7:6

// LSR Register bits
#define LSR_RX_RDY (1 << 0)         // Receive Data Ready Indicator
#define LSR_RX_ORN (1 << 1)         // Receiver Overrun Error Flag
#define LSR_RX_PTY (1 << 2)         // Receive Data Parity Error Tag
#define LSR_RX_FRM (1 << 3)         // Receive Data Framing Error Tag
#define LSR_RX_BRK (1 << 4)         // Receive Break Error Tag
#define LSR_TX_THE (1 << 5)         // Transmit Holding Register Empty Flag
#define LSR_TX_IDL (1 << 6)         // THR and TSR Empty Flag
#define LSR_RX_ERR (1 << 7)         // Receive FIFO Data Error Flag

// EFR Register bits
#define EFR_EN (1 << 4)             // Enable extended features beyond 16C550 compatibility

// DLD Register bits
#define EIGHTX_MODE (1 << 4)        // Bit sampling mode, 8 times
#define FOURX_MODE (1 << 5)         // Bit sampling mode, 4 times

// MCR Register bits
#define MCR_TLR_EN (1 << 2)         // Enable TCR and TLR

// TLR Register bits
#define TLR_TX_OFFSET 0             // Address space spans bits 3:0
#define TLR_RX_OFFSET 4             // Address space spans bits 7:4

// Function prototypes
int init_serial(struct spi_device *spi_device, unsigned int XR20M1170_reset_gpio, int baudrate);
inline extern int serial_transmit_bytes(struct spi_device *spi_device, uint8_t* buf, uint8_t length);
inline extern int enable_tx_interrupt(struct spi_device *spi_device, uint8_t enable);
inline extern ssize_t get_rx_fifo_level(struct spi_device *spi_device);
inline extern int read_rx_bytes(struct spi_device *spi_device, uint8_t* buf, uint8_t readLength);

#endif
