# README #

Auxiliary program for setting various ioctl parameters of the character driver.

### Usage ###

Call with the following arguments to set the baudrate:

    ./main -b <baudrate>

or to set the SPI rate:

    ./main -s <SPI clock rate>


### Version History ###

* Remaining work and features:
- Commands for setting into RS-485 or regular mode, parity and stop bits, etc.
- Cross compiling from Ubuntu.

* Initial commit, version 0.1.2
 Added flush ioctl command.

* Initial commit, version 0.1.1
 Added option to set the SPI clock rate used internally on the board.

* Initial commit, version 0.1.0
 Sets the baudrate.

### How do I get set up? ###

Copy the makefile, main.c, and rs485_ioctl.h from the directory above into a folder on the raspberry pi. Then compile using "make".


### Contribution guidelines ###

* Currently under-going rudimentary structural development, and not quite at the stage where external contributions are required.

### Who do I talk to? ###

* Eirik Taylor at uzzors2k.com
