#!/usr/bin/python2

# Connects to a central server, and runs a speed test
# at the command of the server. Run one instance of this
# script on each end of the RS-485 interface. Make sure
# both are on the same network as the host.
import socket
import select
import sys
import re
import time
import signal
from fcntl import ioctl
import os
from ctypes import *
import struct

charDevicePath = "/dev/rs485_ch"
portNumber = 49152

###############################################################################

testPacketSize = 10000   # Purposely not filling exactly 2^n
testPacket = range(testPacketSize)
for i in range(testPacketSize):
    testPacket[i] = testPacket[i] % 256

def startTransmissionOfTestData():
    print ("Sending test data...")
    totalSentBytes = 0
    temp = bytearray(testPacket)
    while (totalSentBytes < testPacketSize):
        sentBytes = os.write(fd, temp[totalSentBytes:])
        print ("Sent %d data bytes" % (sentBytes))
        totalSentBytes += sentBytes
    print ("Transmission complete!")

def receiveTestData():
    print ("Receiving test data...")
    remainingBytes = testPacketSize
    temp = []
    while (remainingBytes > 0):
        readBytes = os.read(fd, remainingBytes)
        temp.append(readBytes)
        remainingBytes -= len(readBytes)
        print ("Received %d data bytes, remaining %d" % (len(readBytes), remainingBytes))
    rawDataRead = "".join(temp)
    print ("Calculating error rate")
    return compareDataToTestPacket(rawDataRead)

def setBaudrate(baudrateToSet):
    #define RS485IOC_SETBAUD 100
    setIntWithIoctl(2147771392, baudrateToSet)

def setSpiClkRate(spiClkRateToSet):
    #define RS485IOC_SETBAUD 101
    setIntWithIoctl(2147771648, spiClkRateToSet)

def flushBuffers():
    #define RS485IOC_FLUSH 102
    setIntWithIoctl(2147771904, 0)

def setIntWithIoctl(index, intData):
    dataAsMemoryObject = struct.pack("L", intData)
    ioctl(fd, index, dataAsMemoryObject)
    print("Setting ioctl index %d with data %d" % (index, intData))

def compareDataToTestPacket(dataToTest):
    allDataCorrect = True
    for i in range(testPacketSize):
        # Convert from bytestring to actual data
        # Options are native byte order and alignment, unsigned char
        # See https://docs.python.org/3/library/struct.html
        correctedByte = struct.unpack('@B', dataToTest[i])[0]
        bytesMatch = (correctedByte == testPacket[i])
        print ("Read at byte index %d, R%d | E%d, equal %d" % (i, correctedByte, testPacket[i], bytesMatch))
        if not bytesMatch:
            print ("Error at byte index %d, R%d | E%d" % (i, correctedByte, testPacket[i]))
            allDataCorrect = False
    return allDataCorrect

###############################################################################

# Capture interrupt signal (Ctrl-C). Close the TCP connection
def handler(signum, frame):
    os.close(fd)
    server.close()

signal.signal(signal.SIGINT, handler)

if len(sys.argv) != 2:
	print ("Please provide the host IP address as an argument when running this script")
	exit()

# Attempt to initialize the serial device
fd = os.open(charDevicePath, os.O_RDWR)

setSpiClkRate(18000000)  # Set a defined SPI clk rate
flushBuffers();

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
IP_address = str(sys.argv[1])
server.connect((IP_address, portNumber))

haveMasterRole = False

try:
    while True:
        message = server.recv(2048)
        if message:
            digits = int(re.findall("\d+", message)[0])
            # Setting of baud rate
            if "baud" in message:
                setBaudrate(digits)
                server.sendall("ack0")
            # Setting of role
            elif "role" in message:
                haveMasterRole = (digits > 0)
                print("Changing role to %d" % (haveMasterRole))
                server.sendall("ack1")
            # Starting test
            elif "start" in message:
                print("Starting test!")
                if (haveMasterRole):
                    server.sendall("ack2")
                    startTransmissionOfTestData()
                    print("Sending ack for transmit")
                    server.sendall("ack3")
                else:
                    server.sendall("ack4")
                    result = receiveTestData()
                    print("Sending test result " + str(result))
                    server.sendall("test result " + str(result))
            elif "quit" in message:
                print("Exit requested, quitting")
                break
            # Error
            else:
                print ('Error, undefined command!' + message)
                server.sendall("nack")
        else:
            print ('Error waiting for a response!')
            break;

finally:
    os.close(fd)
    server.close()
