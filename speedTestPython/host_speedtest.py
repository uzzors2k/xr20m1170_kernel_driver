#!/usr/bin/python2

# Waits for two clients to connect, and then starts issuing
# synchronized requests for speed tests. Run this script on
# the PC.
import socket
import select
import sys
import time

portNumber = 49152

###############################################################################

def runOneTest(indexMaster, indexSlave):
    # Select one client as the master
    list_of_clients[indexMaster].sendall("setrole 1")
    message = list_of_clients[indexMaster].recv(2048)
    if "ack" not in message:
        print ('Error setting role')
        exit()

    list_of_clients[indexSlave].sendall("setrole 0")
    message = list_of_clients[indexSlave].recv(2048)
    if "ack" not in message:
        print ('Error setting role')
        exit()

    # Start test!
    clientIndex = 0
    for client in list_of_clients:
        client.sendall("start the test 1")
        message = client.recv(2048)
        if "ack" not in message:
            print ('Error starting test for ' + str(clientIndex))
            exit()
        clientIndex = clientIndex + 1

    # Wait for the test to complete, master
    message = list_of_clients[indexMaster].recv(2048)
    if "ack" not in message:
        print ('Error waiting for master to complete test')
        exit()

    # Get results from the slave
    message = list_of_clients[indexSlave].recv(2048)
    if message:
        print ('Test result ' + message)
        if "True" in message:
            return True
    else:
        print ('Error running test')
        exit()

    return False

###############################################################################

# Get runtime parameters
if len(sys.argv) == 3:
    currentBaudRate = int(sys.argv[1])
    baudRateIncrement = int(sys.argv[2])
elif len(sys.argv) == 2:
    currentBaudRate = int(sys.argv[1])
    baudRateIncrement = 10000
else:
    print("NB: Custom starting baud rate, and baud increment can be provided as run time arguments.")
    
    currentBaudRate = 115200
    baudRateIncrement = 10000

print("Starting test with baudrate " + str(currentBaudRate) + " and increment " + str(baudRateIncrement))

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(('', portNumber))   # Don't use localhost here, otherwise it only works on the internal ports
server.listen(2)

list_of_clients = []
while len(list_of_clients) < 2:
	conn, addr = server.accept()
	list_of_clients.append(conn)
	print (addr[0] + " connected")

try:
    errorRateBelowLimit = True
    while errorRateBelowLimit:
        time.sleep(0.3) # Sleep to let everything synchronize between tests

        print("Starting test with baudrate " + str(currentBaudRate))

        # Initialize for new test
        for client in list_of_clients:
            client.sendall("setbaud " + str(currentBaudRate))
            message = client.recv(2048)
            if "ack" not in message:
                print ('Error set baud rate!')
                exit()

        # Run with first set of roles
        master = 0
        slave = 1
        errorRateBelowLimit = runOneTest(master, slave);

        # Swap, and try again
        if (errorRateBelowLimit):
            master = 1
            slave = 0
            errorRateBelowLimit = runOneTest(master, slave);

        # Increment the baudrate to test
        if errorRateBelowLimit:
            currentBaudRate = currentBaudRate + baudRateIncrement

    print("Error rate too high, terminating at baudrate of " + str(currentBaudRate))
    for client in list_of_clients:
        client.sendall("quit 1")

finally:
    for client in list_of_clients:
        client.close()
    server.close()
